﻿using System;

namespace exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
 
            //A:
           var counter=5;
           var a=0;
           var b=0;
           var c=0;
           var index=20;
           var counter2=9;
           var d=2;
            
            // for loop prints 1 - 5 

           for  (var i=1; i<=counter; i++)
            {
            Console.WriteLine($"{i}");
            
            }

            //b while loop prints 1 - 5

            Console.WriteLine();
            while (a <counter)
            {
               b= a+1;
               System.Console.WriteLine($"{b}");
               a++;
            }
            Console.WriteLine();

            //c for loop with even numbers below 20

            for  ( c=0; c<=20; c++)
            {
                if (c%2 ==0)
                {
                   System.Console.WriteLine($"{c}");
                }
            }
            
             Console.WriteLine();

             //d while loop even numbers

           while ( d<=20 )
           {
               
                System.Console.WriteLine($"{d}");
                d+=2;
                
           }

           Console.WriteLine();
           do  
           {
                Console.WriteLine($"index = {index} counter = {counter2}");
                if (index>counter2)
                {
                    System.Console.WriteLine("index is larger than counter");
                }
           }
           
          while (index<counter2);
          {
                Console.ReadKey();
          }
           
            
           
                   
        
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
           
        }
    }
}
